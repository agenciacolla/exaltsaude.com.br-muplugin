<?php
/**
 * Grupo Vify - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   Exalt Saúde
 * @author    Black Magenta <contato@blackmagenta.com.br>
 * @copyright 2022 Black Magenta
 * @license   Proprietary https://blackmagenta.com.br
 * @link      https://blackmagenta.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Exalt Saúde - mu-plugin
 * Plugin URI:  https://blackmagenta.com.br
 * Description: Customizations for exaltsaude.com.br site
 * Version:     1.0.0
 * Author:      Black Magenta
 * Author URI:  https://blackmagenta.com.br/
 * Text Domain: exaltsaudecombr
 * License:     Proprietary
 * License URI: https://blackmagenta.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('exaltsaudecombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor
 */

add_action(
    'admin_head',
    function () {
        $template_file = $template_file = basename( get_page_template() );

        $templates = array("page-politica-de-privacidade.blade.php");
        if (!in_array($template_file, $templates)) { 
            remove_post_type_support('page', 'editor');
        }
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

/**********
 * Show on Front Page
 *
 * @return bool $display
 */
function Show_On_Front_page($cmb)
{
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

    // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($front_page) : $front_page;

    // There is a front page set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Privacy Policy 
 *
 * @return bool $display
 */
function Show_On_Privacy($cmb)
{
    // Get ID of page set as privacy, 0 if there isn't one
    $privacy = get_option('wp_page_for_privacy_policy');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($privacy) : $privacy;

    // There is a privacy set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/***********************************************************************************
 * Page Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_exaltsaude_frontpage_';

        /**
        * Testimonials
        */
        $cmb_testimonials = new_cmb2_box(
            array(
                'id'            => '_exaltsaude_frontpage_testimonials_id',
                'title'         => __('Testimonials', 'exaltsaudecombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Testimonials Group
        $testimonial_id = $cmb_testimonials->add_field(
            array(
                'id'          => $prefix . 'testimonials',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Testimonial {#}', 'exaltsaudecombr'),
                    'add_button'    => __('Add Another Testimonial', 'exaltsaudecombr'),
                    'remove_button' => __('Remove Testimonial', 'exaltsaudecombr'),
                    'sortable'      => true,
                ),
            )
        );

        //Testimonial Photo
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array(
                'name'        => __('Photo', 'exaltsaudecombr'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 320x320px | Aspect ratio: 1:1', 'exaltsaudecombr'),
                'id'          => 'img',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add photo', 'exaltsaudecombr'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(160, 160)
            )
        );

        // Testimonial Name
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Name', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );

        // Testimonial Role
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Role', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => 'role',
                'type'       => 'text_medium',
            )
        );

        // Testimonial Description
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Testimonial', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => 'content',
                'type'       => 'textarea',
            )
        );

        /**
        * Brokers
        */
        $cmb_brokers = new_cmb2_box(
            array(
                'id'            => '_exaltsaude_frontpage_brokers_id',
                'title'         => __('Brokers', 'exaltsaudecombr'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Brokers Logo
        $cmb_brokers->add_field(
            array(
                'name'    => __("Brokers' logos", 'exaltsaudecombr'),
                'desc'    => __('Format: SVG (recommended), PNG | Recommended size: 210x120px | Aspect ratio: 7:4', 'exaltsaudecombr'),
                'id'      => $prefix . 'brokers_logos',
                'type'    => 'file_list',
                'text' => array(
                    'add_upload_files_text' => __('Add or Upload Files', 'exaltsaudecombr'),
                    'remove_image_text'     => __('Remove Image', 'exaltsaudecombr'),
                    'file_text'             => __('File', 'exaltsaudecombr'),
                    'file_download_text'    => __('Download', 'exaltsaudecombr'),
                    'remove_text'           => __('Remove', 'exaltsaudecombr'),
                ),
    
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(210, 120)
            )
        );
    }
);

/**
 * Portal do Corretor
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_exaltsaude_portal_';

        /**
        * Form
        */
        $cmb_portal = new_cmb2_box(
            array(
                'id'            => '_exaltsaude_portal_form_id',
                'title'         => __('Broker Portal', 'exaltsaudecombr'),
                'object_types'  => array('page'), // post type
                'show_on'       => array('key' => 'page-template', 'value' => 'page-portal-do-corretor.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Hash
        $cmb_portal->add_field( 
            array(
                'name'       => __('Hash', 'exaltsaudecombr'),
                'desc'       => __('Your hash parameter must be acquired with Trindade Tecnologia.', 'exaltsaudecombr'),
                'id'         => $prefix . 'hash',
                'type'       => 'text_medium',
            )
        );
    }
);

/**
 * Contato
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_exaltsaude_contato_';
        
        /**
        * Contact
        */
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => '_exaltsaude_contato_contact_id',
                'title'         => __('Contact', 'exaltsaudecombr'),
                'object_types'  => array('page'), // Post type
                'show_on'       => array('key' => 'page-template', 'value' => 'page-contato.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Phone
        $cmb_contact->add_field(
            array(
                'name'       => __('Phone', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_phone',
                'type'       => 'text_medium',
            )
        );

        //Email
        $cmb_contact->add_field(
            array(
                'name'       => __('Email', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_email',
                'type'       => 'text_email',
            )
        );

        //Address
        $cmb_contact->add_field(
            array(
                'name'       => __('Address', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'address',
                'type'       => 'textarea_code',
            )
        );

        //Opening hours
        $cmb_contact->add_field(
            array(
                'name'       => __('Opening hours', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'opening_hours',
                'type'       => 'text',
            )
        );

        //Map (iframe)
        $cmb_contact->add_field(
            array(
                'name'       => __('Map (iframe)', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'map_iframe',
                'type'       => 'textarea_code',
            )
        );

        //Map Footer Link (URL)
        $cmb_contact->add_field(
            array(
                'name'       => __('Map Link (URL)', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'map_url',
                'type'       => 'text_url',
            )
        );

        /**
        * Form
        */
        $cmb_form = new_cmb2_box(
            array(
                'id'            => '_exaltsaude_contato_contact_form_id',
                'title'         => __('Contact Form', 'exaltsaudecombr'),
                'object_types'  => array('page'), // Post type
                'show_on'       => array('key' => 'page-template', 'value' => 'page-contato.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Form Shortcode
        $cmb_form->add_field(
            array(
                'name'       => __('Contact Form Shortcode', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_form_shortcode',
                'type'       => 'text',
            )
        );

        /**
        * Social Networks
        */
        $cmb_social = new_cmb2_box(
            array(
                'id'            => '_exaltsaude_contato_social_id',
                'title'         => __('Social Networks', 'exaltsaudecombr'),
                'object_types'  => array('page'), // Post type
                'show_on'       => array('key' => 'page-template', 'value' => 'page-contato.blade.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
            )
        );

        //Facebook (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Facebook (URL)', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_facebook_url',
                'type'       => 'text_url',
            )
        );

        //Instagram (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Instagram (URL)', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_instagram_url',
                'type'       => 'text_url',
            )
        );

        //LinkedIn (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('LinkedIn (URL)', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_linkedin_url',
                'type'       => 'text_url',
            )
        );

        //Youtube (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Youtube (URL)', 'exaltsaudecombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_youtube_url',
                'type'       => 'text_url',
            )
        );
    }
);
