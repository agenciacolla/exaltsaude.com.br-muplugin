��    $      <  5   \      0     1     I  	   ]     g     o     }     �     �     �     �     �     �     �     �  P   �  P   4     �     �     �     �     �     �     �     �     �     �     �     �                     ,     <  >   I     �  h  �     �          6  	   E     O  
   b     m     �     �  #   �     �     �     �     �  S   �  S   ?     �     �     �     �     �     �     �     �      	     	     	     	     /	     5	  
   C	     N	     ]	  A   i	     �	                                                      $                           #       
                               "                  	                                !           Add Another Testimonial Add or Upload Files Add photo Address Broker Portal Brokers Brokers' logos Contact Contact Form Contact Form Shortcode Download Email Facebook (URL) File Format: JPG (recommended), PNG | Recommended size: 320x320px | Aspect ratio: 1:1 Format: SVG (recommended), PNG | Recommended size: 210x120px | Aspect ratio: 7:4 Hash Instagram (URL) LinkedIn (URL) Map (iframe) Map Link (URL) Name Opening hours Phone Photo Remove Remove Image Remove Testimonial Role Social Networks Testimonial Testimonial {#} Testimonials Your hash parameter must be acquired with Trindade Tecnologia. Youtube (URL) Project-Id-Version: 
PO-Revision-Date: 2022-08-03 15:42-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.1.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: exaltsaudecombr.php
 Adicionar Novo Depoimento Adicionar ou Enviar Arquivos Adicionar foto Endereço Portal do Corretor Corretoras Logo das Corretoras Contato Formulário de Contato Shortcode do Formulário de Contato Download E-mail Facebook (URL) Arquivo Formato: JPG (recomendado), PNG | Tamanho recomendado: 320x320px | Proporção: 1:1 Formato: SVG (recomendado), PNG | Tamanho recomendado: 210x120px | Proporção: 7:4 Hash Instagram (URL) LinkedIn (URL) Mapa (iframe) Link do Mapa (URL) Nome Horário de funcionamento Telefone Foto Remover Remover Imagem Remover Depoimento Cargo Redes Sociais Depoimento Depoimento {#} Depoimentos Seu parâmetro hash deve ser adquirido com a Trindade Tecnologia. Youtube (URL) 